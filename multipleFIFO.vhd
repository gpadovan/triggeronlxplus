-- project multipleFIFO
-- multipleFIFO.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operation with std_logic_vector
use ieee.math_real.uniform; -- to extract uniform random numbers in [0,1)
use ieee.math_real.floor; -- to do floor (parte intera) operation
use ieee.numeric_std.all;

library xil_defaultlib;
use xil_defaultlib.my_lib.all;

entity multipleFIFO is
    Port ( global_write_en : in std_logic;
           global_read_en : in std_logic;
           data_in : in std_logic_vector(18 downto 0);
           data_out : out data_bus;
           reset : in std_logic;
           clock : in std_logic;
           SL_BCID : in std_logic_vector(8 downto 0);
           address_in : in std_logic_vector(8 downto 0);
           mFIFO_empty : out std_logic_vector(511 downto 0);
           mFIFO_read_en : in std_logic_vector(511 downto 0) );
end multipleFIFO;

architecture Behavioral of multipleFIFO is

signal FIFO_write_en : std_logic_vector(511 downto 0) := (others => '0');
signal address_in_dec : std_logic_vector(511 downto 0) := (others => '0');
signal data_in_del1clk : std_logic_vector(18 downto 0) := (others => '0');
signal data_in_del2clk : std_logic_vector(18 downto 0) := (others => '0');
signal data_in_del3clk : std_logic_vector(18 downto 0) := (others => '0');
signal global_write_en_del1clk : std_logic := '0';
signal global_write_en_del2clk : std_logic := '0';
signal nonNullData : std_logic := '0';
signal address_rst : std_logic_vector(8 downto 0) := (others => '0');
signal address_rst_dec : std_logic_vector(511 downto 0) := (others => '0');
signal mFIFO_rst : std_logic_vector(511 downto 0) := (others => '0');
signal FIFO_full : std_logic_vector(511 downto 0);

component singleFIFO
    Port ( wr_en : in std_logic;
           rd_en : in std_logic;
           din : in std_logic_vector(18 downto 0);
           dout : out std_logic_vector(18 downto 0);
           rst : in std_logic;
           clk : in std_logic;
           full : out std_logic;
           empty : out std_logic );
end component;


component decoder
    Port ( sgn_enc : in std_logic_vector(8 downto 0);
           clock : in std_logic;
           sgn_dec : out std_logic_vector(511 downto 0) );
end component;

begin

gen_FIFO : for I in 0 to 511 generate
    FIFO : singleFIFO port map (
        wr_en => FIFO_write_en(I),
        rd_en => mFIFO_read_en(I),
        din => data_in_del3clk,
        dout => data_out(I),
        rst => mFIFO_rst(I),
        clk => clock,
        full => FIFO_full(I),
        empty => mFIFO_empty(I) );
end generate gen_FIFO;

pmap_decoder_in : decoder port map (
    sgn_enc => address_in,
    clock => clock,
    sgn_dec => address_in_dec );
    
pmap_decoder_rst : decoder port map (
    sgn_enc => address_rst,
    clock => clock,
    sgn_dec => address_rst_dec );
    

FIFO_write_en_gen : process(clock)
begin
    if rising_edge(clock) then
        for J in 511 downto 0 loop
            FIFO_write_en(J) <= global_write_en_del2clk and address_in_dec(J) and nonNullData;
        end loop;
    end if;
end process;

data_in_del1clk_gen : process(clock)
begin
    if rising_edge(clock) then
        data_in_del1clk <= data_in;
    end if;
end process;

data_in_del2clk_gen : process(clock)
begin
    if rising_edge(clock) then
        data_in_del2clk <= data_in_del1clk;
    end if;
end process;

data_in_del3clk_gen : process(clock)
begin
    if rising_edge(clock) then
        data_in_del3clk <= data_in_del2clk;
    end if;
end process;

global_write_en_del1clk_gen : process(clock)
begin
    if rising_edge(clocK) then
        global_write_en_del1clk <= global_write_en;
    end if;
end process;

global_write_en_del2clk_gen : process(clock)
begin
    if rising_edge(clock) then
        global_write_en_del2clk <=  global_write_en_del1clk;
    end if;
end process;


nonNullData_gen : process(clock)
begin
    if rising_edge(clock) then
        if data_in_del1clk /= "000000000000000000" then
            nonNullData <= '1';
        else
            nonNullData <= '0';
        end if;
    end if;
end process;


address_rst_gen : process(clock)
begin
    if rising_edge(clock) then
        address_rst <= SL_BCID - std_logic_vector(to_unsigned(480, address_rst'length));
    end if;
end process;

mFIFO_rst_gen : process(clock)
begin
    if rising_edge(clock) then
        for I in 511 downto 0 loop
            mFIFO_rst(I) <= reset or address_rst_dec(I);
        end loop;
    end if;
end process;


end Behavioral;
