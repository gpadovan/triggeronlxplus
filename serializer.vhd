-- project multipleFIFO
-- serializer.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use ieee.numeric_std.all;

library xil_defaultlib;
use xil_defaultlib.my_lib.all;

entity serializer is
    Port ( clock : in std_logic;
           L0A : in std_logic;
           SL_BCID : in std_logic_vector(8 downto 0);
           mFIFO_empty : in std_logic_vector(511 downto 0);
           mFIFO_read_en : out std_logic_vector(511 downto 0);
           data_in : in data_bus );
    
end serializer;

architecture Behavioral of serializer is

type state_type is (idle, ST0);
signal CS : state_type := idle;
signal address_out : std_logic_vector(8 downto 0) := (others => '0');
signal address_out_dec : std_logic_vector(511 downto 0) := (others => '0');
signal L0A_del1clk : std_logic := '0';
signal L0A_del2clk : std_logic := '0';


component decoder
    Port ( sgn_enc : in std_logic_vector(8 downto 0);
           clock : in std_logic;
           sgn_dec : out std_logic_vector(511 downto 0) );
end component;


begin

pmap_decoder_out : decoder port map (
    sgn_enc => address_out,
    clock => clock,
    sgn_dec => address_out_dec );

seq_proc : process(clock)
begin
    if rising_edge(clock) then
        case CS is
            when idle =>
                mFIFO_read_en <= (others => '0');
                if L0A_del2clk = '1' and mFIFO_empty(to_integer(unsigned(address_out))) = '0' then
                    CS <= ST0;
                end if;
            when ST0 =>
                mFIFO_read_en <= address_out_dec;
                --for I in 511 downto 0 loop
                --     if I = to_integer(unsigned(address_out)) then
                --        if mFIFO_empty(I) = '1' then
                --            CS <= idle;
                --        end if;
                --    end if;
                --end loop;
                
                if mFIFO_empty(to_integer(unsigned(address_out))) = '1' then
                    CS <= idle;
                end if;
                
        end case;
    end if;
end process;


address_out_gen : process(clock)
begin
    if rising_edge(clock) then
        address_out <= SL_BCID - std_logic_vector(to_unsigned(400, address_out'length));
    end if;
end process;

L0A_del1clk_gen : process(clock)
begin
    if rising_edge(clock) then
        L0A_del1clk <= L0A;
    end if;
end process;
    
L0A_del2clk_gen : process(clock)
begin
    if rising_edge(clock) then
        L0A_del2clk <= L0A_del1clk;
    end if;
end process;

    
end Behavioral;
