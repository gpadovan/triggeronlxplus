-- project SLReadout (created 21-11-2021)
-- multipleFIFO.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operation with std_logic_vector
use ieee.math_real.uniform; -- to extract uniform random numbers in [0,1)
use ieee.math_real.floor; -- to do floor (parte intera) operation
use ieee.numeric_std.all;

library xil_defaultlib;
use xil_defaultlib.my_library.all;


entity multipleFIFO is
  Port ( clock : in std_logic;
         reset : in std_logic;
         ready_address : in std_logic_vector(8 downto 0);
         global_write_en : in std_logic;
         data_in : std_logic_vector(18 downto 0);
         address_in : std_logic_vector(8 downto 0);
         address_rst : std_logic_vector(8 downto 0) );
end multipleFIFO;

architecture Behavioral of multipleFIFO is
signal multiple_data_out : data_bus;
signal TMP_FIFO_full : std_logic_vector(511 downto 0) := (others => '0');
signal TMP_FIFO_empty : std_logic_vector(511 downto 0) := (others => '0');
signal FIFO_write_en : std_logic_vector(511 downto 0) := (others => '0');
signal FIFO_rst : std_logic_vector(511 downto 0) := (others => '0');
signal TMP_FIFO_read_en : std_logic := '0';
signal address_in_dec : std_logic_vector(511 downto 0) := (others => '0');
signal address_rst_dec : std_logic_vector(511 downto 0) := (others => '0');
signal data_in_del1clk : std_logic_vector(18 downto 0) := (others => '0');
signal data_in_del2clk : std_logic_vector(18 downto 0) := (others => '0');
signal nonNullData : std_logic := '0';
signal global_write_en_del1clk : std_logic := '0';

component singleFIFO
    Port ( wr_en : in std_logic;
           rd_en : in std_logic;
           din : in std_logic_vector(18 downto 0);
           dout : out std_logic_vector(18 downto 0);
           rst : in std_logic;
           clk : in std_logic;
           full : out std_logic;
           empty : out std_logic );
end component;

component decoder
    Port ( clock : in std_logic;
           sgn_enc : in std_logic_vector(8 downto 0);
           sgn_dec : out std_logic_vector(511 downto 0) );
end component;



begin

gen_FIFO : for I in 0 to 511 generate
    FIFO : singleFIFO port map (
        wr_en => FIFO_write_en(I),
        rd_en => TMP_FIFO_read_en,
        din => data_in_del2clk,
        dout => multiple_data_out(I),
        rst => FIFO_rst(I),
        clk => clock,
        full => TMP_FIFO_full(I),
        empty => TMP_FIFO_empty(I) );
end generate gen_FIFO;

pmap_decoder_in : decoder port map (
    clock => clock,
    sgn_enc => address_in,
    sgn_dec => address_in_dec );

pmap_decoder_rst : decoder port map (
    clock => clock,
    sgn_enc => address_rst,
    sgn_dec => address_rst_dec );

FIFO_write_en_gen : process(clock)
begin
    if rising_edge(clock) then
        for I in 0 to 511 loop
            FIFO_write_en(I) <= global_write_en_del1clk and address_in_dec(I) and nonNullData;
        end loop;
    end if;
end process;

FIFO_rst_gen : process(clock)
begin
    if rising_edge(clock) then
        for I in 0 to 511 loop
            FIFO_rst(I) <= reset or address_rst_dec(I);
        end loop;
    end if;
end process;

data_in_del1clk_gen : process(clock)
begin
    if rising_edge(clock) then
        data_in_del1clk <= data_in;
    end if;
end process;

data_in_del2clk_gen : process(clock)
begin
    if rising_edge(clock) then
        data_in_del2clk <= data_in_del1clk;
    end if;
end process;

nonNullData_gen : process(clock)
begin
    if rising_edge(clock) then
        if data_in /= "0000000000000000000" then
            nonNullData <= '1';
        else 
            nonNullData <= '0';
        end if;
    end if;
end process;

global_write_en_del1clk_gen : process(clock)
begin
    if rising_edge(clock) then
        global_write_en_del1clk <= global_write_en;
    end if;
end process;

  

end Behavioral;
