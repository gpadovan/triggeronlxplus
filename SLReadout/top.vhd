-- project: SLReadout (created 21-11-21)
-- top.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operations on std_logic_vector
use IEEE.numeric_std.all;

library xil_defaultlib; -- FIFO library
use xil_defaultlib.my_library.all;

library work;
use work.all;


entity top is
  Port (clock_400MHz : in std_logic;
        clock_40MHz : in std_logic;
        enable : in std_logic;
        reset : in std_logic;
        data_in : in std_logic_vector(27 downto 0); 
        L0A : in std_logic  );
end top;


architecture Behavioral of top is

signal wait_FIFO_L0A_write_en : std_logic := '1';
signal wait_FIFO_L0A_read_en : std_logic := '1';
signal wait_write_en : std_logic := '1';
signal FIFO_L0A_write_en : std_logic := '0';
signal FIFO_L0A_read_en : std_logic := '0';
signal write_en : std_logic := '0';
signal trig_address : std_logic_vector(8 downto 0) := (others => '0');
signal ready_address : std_logic_vector(8 downto 0) := (others => '0');
signal FIFO_L0A_full : std_logic := '0';
signal FIFO_L0A_empty : std_logic := '0';
signal SL_BCID : std_logic_vector(8 downto 0) := (others => '1');
signal address_rst : std_logic_vector(8 downto 0) := (others => '0');



component FIFO_L0A
    Port ( wr_en : in std_logic;
           rd_en : in std_logic;
           din : in std_logic_vector(8 downto 0);
           dout : out std_logic_vector(8 downto 0);
           rst : in std_logic;
           clk : in std_logic;
           full : out std_logic;
           empty : out std_logic );
end component;

component multipleFIFO
    Port ( clock : in std_logic;
           reset : in std_logic;
           ready_address : in std_logic_vector(8 downto 0);
           global_write_en : in std_logic;
           data_in : std_logic_vector(18 downto 0);
           address_in : std_logic_vector(8 downto 0);
           address_rst : std_logic_vector(8 downto 0) );
end component;

component serializer
    Port ( clock : in std_logic );
end component;      

begin

  pmap_FIFO_L0A : FIFO_L0A port map (
      wr_en => FIFO_L0A_write_en,
      rd_en => FIFO_L0A_read_en,
      din => trig_address,
      dout => ready_address,
      rst => reset,
      clk => clock_400MHz,
      full=> FIFO_L0A_full,
      empty => FIFO_L0A_empty );
      
  pmap_multipleFIFO : multipleFIFO port map (
      clock => clock_400MHz,
      reset => reset,
      ready_address => ready_address,
      global_write_en => write_en,
      data_in => data_in(18 downto 0),
      address_in => data_in(27 downto 19),
      address_rst => address_rst );
  
  pmap_serializer : serializer port map (
      clock => clock_400MHz );
           
  FIFO_L0A_write_en_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
      if reset = '1' or wait_FIFO_L0A_write_en = '1' or L0A = '0' then
        FIFO_L0A_write_en <= '0';
      else
        FIFO_L0A_write_en <= '1';
      end if;
    end if;
  end process;

  FIFO_L0A_read_en_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
        if reset = '1' or wait_FIFO_L0A_read_en = '1' then
            FIFO_L0A_read_en <= '0';
        else
            FIFO_L0A_read_en <= '1';
        end if;
    end if;
  end process;
  
  wait_FIFO_L0A_write_en_gen : process
  begin
    wait_FIFO_L0A_write_en <= '1';
    wait for 500 ns;
    wait_FIFO_L0A_write_en <= '0';
    wait;
  end process;  
  
  wait_FIFO_L0A_read_en_gen : process
  begin
    wait_FIFO_L0A_read_en <= '1';
    wait for 500 ns;
    wait_FIFO_L0A_read_en <= '0';
    wait;
  end process;
    
  write_en_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
        if reset = '1' or wait_write_en = '1' then
            write_en <= '0';
        else
            write_en <= '1';
        end if;
    end if;
  end process; 
  
  wait_write_en_gen : process
  begin
    wait_write_en <= '1';
    wait for 500 ns;
    wait_write_en <= '0';
    wait;
  end process;    

  SL_BCID_gen : process(clock_40MHz)
  begin
    if rising_edge(clock_40MHz) then
      if enable = '1' then
        SL_BCID <= SL_BCID + 1;
      end if;
    end if;
  end process;
  
  trig_address_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
      trig_address <= SL_BCID - std_logic_vector(to_unsigned(400, trig_address'length));
    end if;
  end process;
  
  address_rst_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
        address_rst <= SL_BCID - std_logic_vector(to_unsigned(480, address_rst'length));
    end if;
  end process;
  


end Behavioral;
