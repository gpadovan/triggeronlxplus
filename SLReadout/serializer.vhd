-- project SLReadout (created 21-11-2021)
-- serializer.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use ieee.numeric_std.all;

library xil_defaultlib;
use xil_defaultlib.my_library.all;


entity serializer is
    Port ( clock : in std_logic );
end serializer;

architecture Behavioral of serializer is

type state_type is (idle, ST0);
signal CS : state_type := idle;

begin

seq_proc : process(clock)
begin
    if rising_edge(clock) then
        case CS is
            when idle =>
            when ST0 =>
        end case;
    end if;
end process;

end Behavioral;
