-- project SLReadout (created 21-11-2021)
-- my_library.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package my_library is 
    type data_bus is array (0 to 511) of std_logic_vector(18 downto 0);
end package;

package body my_library is

end my_library;