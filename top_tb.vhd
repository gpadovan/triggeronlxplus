-- project: multipleFIFO
-- top_tb.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operation with std_logic_vector

use ieee.math_real.uniform; -- to extract uniform random numbers in [0,1)
use ieee.math_real.floor; -- to do floor (parte intera) operation
use ieee.numeric_std.all;


entity top_tb is
--  Port ( );
end top_tb;

architecture Behavioral of top_tb is

--clock signals
  signal tb_clock_400MHz : std_logic;
  signal tb_clock_40MHz : std_logic;
  signal tb_clock_1MHz : std_logic;

  signal tb_reset : std_logic;
  signal tb_out : std_logic_vector(31 downto 0) := (others => '0');
  signal dg_enable : std_logic := '0';
  
  signal DCT_data : std_logic_vector(27 downto 0) := (others=>'0');
  signal L0A : std_logic := '0';
  

  
  component top
    Port (clock_400MHz : in std_logic;
          clock_40MHz : in std_logic;
          reset : in std_logic;
          enable : in std_logic;
          data_in : in std_logic_vector(27 downto 0);
          L0A : in std_logic;
          top_out : out std_logic_vector(31 downto 0) );
  end component;
  
  component data_generator
    Port (clock : in std_logic;
          enable : in std_logic;
          data_out : out std_logic_vector(27 downto 0) );
  end component;
  
  component L0A_generator
    Port (clock : in std_logic;
          trigger_rate : in std_logic;
          L0A : out std_logic );
  end component;


begin
  pmap_top : top port map (
    clock_400MHz => tb_clock_400MHz,
    clock_40MHz => tb_clock_40MHz,
    reset => tb_reset,
    enable => dg_enable,
    data_in => DCT_data,
    L0A => L0A,
    top_out => tb_out );
    
  pmap_data_generator : data_generator port map (
    clock => tb_clock_400MHz,
    enable => dg_enable,
    data_out => DCT_data );

  pmap_L0A_generator : L0A_generator port map (
    clock => tb_clock_400MHz,
    trigger_rate => tb_clock_1MHz,
    L0A => L0A );
          
    -- generation of clock at 400 MHz  
    clock_400MHz_gen : process
    begin
        tb_clock_400MHz <= '1';
        wait for 1.25 ns;
        tb_clock_400MHz <= '0';
        wait for 1.25 ns;
    end process;
    
    -- generation of clock at 40 MHz
    clock_40MHz_gen : process
    begin
        tb_clock_40MHz <= '1';
        wait for 12.5 ns;
        tb_clock_40MHz <= '0';
        wait for 12.5 ns;
    end process;
    
    clock_1MHz_gen : process
    begin
        tb_clock_1MHz <= '1';
        wait for 0.5 us;
        tb_clock_1MHz <= '0';
        wait for 0.5 us;
    end process;


    -- generation of reset signal
    reset_gen : process
    begin
      tb_reset <= '1';
      wait for 100 ns;
      tb_reset <= '0';
      wait;
    end process;

    
    dg_enable_gen : process
    begin
        dg_enable <= '0';
        wait for 500 ns;
        dg_enable <= '1';
        wait;
    end process;
    

end Behavioral;
