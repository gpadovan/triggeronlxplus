create_project SL /data/gpadovan/vivado_projects/SLReadout/SL -part xcvu13p-flga2577-1-e
set_property target_language VHDL [current_project]

add_files -norecurse /data/gpadovan/vivado_projects/SLReadout/RTL/decoder.vhd
add_files -norecurse /data/gpadovan/vivado_projects/SLReadout/RTL/decoder_2bit.vhd
add_files -norecurse /data/gpadovan/vivado_projects/SLReadout/RTL/serializer.vhd
add_files -norecurse /data/gpadovan/vivado_projects/SLReadout/RTL/top.vhd
add_files -norecurse /data/gpadovan/vivado_projects/SLReadout/RTL/my_library.vhd
add_files -norecurse /data/gpadovan/vivado_projects/SLReadout/RTL/multipleFIFO.vhd
add_files -norecurse /data/gpadovan/vivado_projects/SLReadout/RTL/memory_block.vhd


add_files -norecurse /data/gpadovan/vivado_projects/SLReadout/IP/singleFIFO/singleFIFO.xci
add_files -norecurse /data/gpadovan/vivado_projects/SLReadout/IP/FIFO_L0A/FIFO_L0A.xci
add_files -norecurse /data/gpadovan/vivado_projects/SLReadout/IP/FIFO_to_felix/FIFO_to_felix.xci

set_property SOURCE_SET sources_1 [get_filesets sim_1]

add_files -fileset sim_1 -norecurse /data/gpadovan/vivado_projects/SLReadout/sim/top_tb.vhd
add_files -fileset sim_1 -norecurse /data/gpadovan/vivado_projects/SLReadout/sim/data_generator.vhd
add_files -fileset sim_1 -norecurse /data/gpadovan/vivado_projects/SLReadout/sim/L0A_generator.vhd
