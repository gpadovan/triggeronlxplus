-- project SLReadout (created 21-11-2021)
-- my_library.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package my_library is 
    type data_bus is array (511 downto 0) of std_logic_vector(18 downto 0);
    type data_bus_26 is array (25 downto 0) of std_logic_vector(18 downto 0);
    type address_bus_3 is array (2 downto 0) of std_logic_vector(8 downto 0);
    type address_bus_26 is array (15 downto 0) of std_logic_vector(8 downto 0);
end package;

package body my_library is

end my_library;