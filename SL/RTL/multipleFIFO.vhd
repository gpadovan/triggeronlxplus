-- project SLReadout (created 21-11-2021)
-- multipleFIFO.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operation with std_logic_vector
use ieee.math_real.uniform; -- to extract uniform random numbers in [0,1)
use ieee.math_real.floor; -- to do floor (parte intera) operation
use ieee.numeric_std.all;

library xil_defaultlib;
use xil_defaultlib.my_library.all;


entity multipleFIFO is
  Port ( clock : in std_logic;
         reset : in std_logic;
         address_out: in std_logic_vector(8 downto 0);
         global_write_en : in std_logic;
         data_in : in std_logic_vector(18 downto 0);
         address_in : in std_logic_vector(8 downto 0);
         address_rst : in std_logic_vector(8 downto 0);
         multipleFIFO_read_en : in std_logic;
         multipleFIFO_FIFO_empty : out std_logic;
         multipleFIFO_data_out : out std_logic_vector(18 downto 0) );
end multipleFIFO;

architecture Behavioral of multipleFIFO is
signal data_out : data_bus;
signal TMP_FIFO_full : std_logic_vector(511 downto 0);
signal FIFO_empty : std_logic_vector(511 downto 0);
signal FIFO_write_en : std_logic_vector(511 downto 0) := (others => '0');
signal FIFO_rst : std_logic_vector(511 downto 0) := (others => '0');
signal FIFO_read_en : std_logic_vector(511 downto 0);
signal address_in_dec : std_logic_vector(511 downto 0) := (others => '0');
signal address_rst_dec : std_logic_vector(511 downto 0) := (others => '0');
signal address_out_dec : std_logic_vector(511 downto 0) := (others => '0');
signal data_in_del1clk : std_logic_vector(18 downto 0) := (others => '0');
signal data_in_del2clk : std_logic_vector(18 downto 0) := (others => '0');
signal nonNullData : std_logic := '0';
signal global_write_en_del1clk : std_logic := '0';
signal multipleFIFO_read_en_del1clk : std_logic;
--signal multipleFIFO_data_out : std_logic_vector(18 downto 0);





signal TMP_wr_rst_busy : std_logic;
signal TMP_rd_rst_busy : std_logic;

component singleFIFO
    Port ( clk : IN STD_LOGIC;
           srst : IN STD_LOGIC;
           din : IN STD_LOGIC_VECTOR(18 DOWNTO 0);
           wr_en : IN STD_LOGIC;
           rd_en : IN STD_LOGIC;
           dout : OUT STD_LOGIC_VECTOR(18 DOWNTO 0);
           full : OUT STD_LOGIC;
           empty : OUT STD_LOGIC;
           wr_rst_busy : OUT STD_LOGIC;
           rd_rst_busy : OUT STD_LOGIC );
end component;

component decoder
    Port ( clock : in std_logic;
           sgn_enc : in std_logic_vector(8 downto 0);
           sgn_dec : out std_logic_vector(511 downto 0) );
end component;



begin

gen_FIFO : for I in 0 to 511 generate 
    FIFO : singleFIFO port map (
        clk => clock,
        srst => FIFO_rst(I),
        din => data_in_del2clk,
        wr_en => FIFO_write_en(I),
        rd_en => FIFO_read_en(I),
        dout => data_out(I),
        full => TMP_FIFO_full(I),
        empty => FIFO_empty(I),
        wr_rst_busy => TMP_wr_rst_busy,
        rd_rst_busy => TMP_rd_rst_busy );
end generate gen_FIFO;

pmap_decoder_in : decoder port map (
    clock => clock,
    sgn_enc => address_in,
    sgn_dec => address_in_dec );

pmap_decoder_rst : decoder port map (
    clock => clock,
    sgn_enc => address_rst,
    sgn_dec => address_rst_dec );
    
pmap_decoder_out : decoder port map (
    clock => clock,
    sgn_enc => address_out,
    sgn_dec => address_out_dec );

FIFO_write_en_gen : process(clock)
begin
    if rising_edge(clock) then
        for I in 0 to 511 loop
            FIFO_write_en(I) <= global_write_en_del1clk and address_in_dec(I) and nonNullData;
        end loop;
    end if;
end process;

FIFO_read_en_gen : process(clock)
begin
    if rising_edge(clock) then
        for I in 0 to 511 loop
            FIFO_read_en(I) <= multipleFIFO_read_en_del1clk and address_out_dec(I) and not FIFO_empty(I);
        end loop;
    end if;
end process;

FIFO_rst_gen : process(clock)
begin
    if rising_edge(clock) then
        for I in 0 to 511 loop
            FIFO_rst(I) <= reset or address_rst_dec(I);
        end loop;
    end if;
end process;


data_in_del1clk_gen : process(clock)
begin
    if rising_edge(clock) then
        data_in_del1clk <= data_in;
    end if;
end process;

data_in_del2clk_gen : process(clock)
begin
    if rising_edge(clock) then
        data_in_del2clk <= data_in_del1clk;
    end if;
end process;

nonNullData_gen : process(clock)
begin
    if rising_edge(clock) then
        if data_in /= "0000000000000000000" then
            nonNullData <= '1';
        else 
            nonNullData <= '0';
        end if;
    end if;
end process;

global_write_en_del1clk_gen : process(clock)
begin
    if rising_edge(clock) then
        global_write_en_del1clk <= global_write_en;
    end if;
end process;

multipleFIFO_read_en_del1clk_gen : process(clock)
begin
    if rising_edge(clock) then
        multipleFIFO_read_en_del1clk <= multipleFIFO_read_en;
    end if;
end process;

multiplexer_FIFO_empty : process(clock)
begin
    if rising_edge(clock) then
        for I in 0 to 511 loop
            if I = to_integer(unsigned(address_out)) then
                multipleFIFO_FIFO_empty <= FIFO_empty(I);
            end if;
        end loop; 
    end if;
end process;

multiplexer_data : process(clock)
begin
    if rising_edge(clock) then
        for I in 0 to 511 loop
            if I = to_integer(unsigned(address_out)) then
                multipleFIFO_data_out <= data_out(I);
            end if;
        end loop;
    end if;
end process;
                



end Behavioral;
