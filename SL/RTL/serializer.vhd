-- project SLReadout (created 21-11-2021)
-- serializer.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use ieee.numeric_std.all;

library xil_defaultlib;
use xil_defaultlib.my_library.all;


entity serializer is
    Port ( clock : in std_logic;
           FIFO_L0A_empty : in std_logic;
           ser_FIFO_L0A_read_en : out std_logic;
           multipleFIFO_address : out std_logic_vector(4 downto 0);
           memory_block_read_en : out std_logic;
           SL_BCID : in std_logic_vector(8 downto 0);
           memory_block_FIFO_empty : in std_logic ); -- temporaneo DA CANCELLARE
end serializer;

architecture Behavioral of serializer is
signal multipleFIFO_counter : std_logic_vector(4 downto 0) := (others=> '0');
signal count_ST0 : std_logic_vector(1 downto 0) := (others => '0');

type state_type is (idle, ST0, ST1, ST2);
signal CS : state_type := idle;

begin

--seq_proc : process(clock)
--begin
--    if rising_edge(clock) then
--        case CS is
--            when idle =>
--                memory_block_read_en <= '0';
--                multipleFIFO_counter <= (others => '0');
--                ser_FIFO_L0A_read_en <= '0';
--                if FIFO_L0A_empty = '0' then
--                    ser_FIFO_L0A_read_en <= '1';
--                    CS <= ST0;
--                end if;
--            when ST0 =>
--                ser_FIFO_L0A_read_en <= '0';
--                memory_block_read_en <= '1';
--                if memory_block_FIFO_empty = '1' then
--                    CS <= ST1;
--                end if;
--                --if to_integer(unsigned(SL_BCID)) >= 420 then
--                --    CS <= ST0;
--                --else
--                --    CS <= ST1;
--                --end if;
--            when ST1 =>
--                multipleFIFO_counter <= multipleFIFO_counter + 1;
--                if to_integer(unsigned(multipleFIFO_counter)) = 2 then 
--                    CS <= idle;
--                else 
--                    CS <= ST0;
--                end if;
--
--        end case;
--    end if;
--end process;

-- VERSIONE CON PASSAGGIO DI STATO REPENTINO E CORRETTA ESTRAZIONE DEI PACCHETTI (65 ECC ECC)
--seq_proc : process(clock)
--begin
--    if rising_edge(clock) then
--        case CS is
--            when idle =>
--                memory_block_read_en <= '0';
--                multipleFIFO_counter <= (others => '0');
--                ser_FIFO_L0A_read_en <= '0';
--                if FIFO_L0A_empty = '0' then
--                    ser_FIFO_L0A_read_en <= '1';
--                    CS <= ST0;
--                end if;
--            when ST0 =>
--                ser_FIFO_L0A_read_en <= '0';
--                memory_block_read_en <= '1';
--                CS <= ST1;
--            when ST1 =>
--                CS <= idle;
--        end case;
--    end if;
--end process;

-- prima di modifiche Lorenzo
seq_proc : process(clock)
begin
    if rising_edge(clock) then
        case CS is
            when idle =>
                memory_block_read_en <= '0';
                multipleFIFO_counter <= (others => '0');
                ser_FIFO_L0A_read_en <= '0';
                if FIFO_L0A_empty = '0' then
                    ser_FIFO_L0A_read_en <= '1';
                    count_ST0 <= (others => '0');
                    CS <= ST0;
                end if;
            when ST0 =>
                ser_FIFO_L0A_read_en <= '0';
                if to_integer(unsigned(count_ST0)) < 3 then
                    count_ST0 <= count_ST0 + 1;
                    CS <= ST0;
                else
                    CS <= ST1;
                end if;
            when ST1 =>
                memory_block_read_en <= '1';
                if memory_block_FIFO_empty = '1' then
                    CS <= ST2;
                else
                    CS <= ST1;
                end if;
            when ST2 =>                
                multipleFIFO_counter <= multipleFIFO_counter + 1;
                if to_integer(unsigned(multipleFIFO_counter)) = 25 then 
                    CS <= idle;
                else
                    count_ST0 <= (others => '0');
                    memory_block_read_en <= '0';
                    CS <= ST0;
                end if;
        end case;
    end if;
end process;


--seq_proc : process(clock)
--begin
--    if rising_edge(clock) then
--        case CS is
--            when idle =>
--                memory_block_read_en <= '0';
--                multipleFIFO_counter <= (others => '1');
--                ser_FIFO_L0A_read_en <= '0';
--                if FIFO_L0A_empty = '0' then
--                    ser_FIFO_L0A_read_en <= '1';
--                    count_ST0 <= (others => '0');
--                    multipleFIFO_counter <= multipleFIFO_counter + 1;
--                    CS <= ST0;
--                end if;
--            when ST0 =>
--                ser_FIFO_L0A_read_en <= '0';
--                if to_integer(unsigned(count_ST0)) < 3 then
--                    count_ST0 <= count_ST0 + 1;
--                    CS <= ST0;
--                else
--                    CS <= ST1;
--                end if;
--            when ST1 =>
--                memory_block_read_en <= '1';
--                if memory_block_FIFO_empty = '1' then
--                    if to_integer(unsigned(multipleFIFO_counter)) = 4 then -- svuota fino a 25
--                        CS <= idle;
--                    else
--                        multipleFIFO_counter <= multipleFIFO_counter + 1;
--                        CS <= ST0;
--                    end if;
--                else
--                    CS <= ST1;
--                end if;
----            when ST2 =>                
----                multipleFIFO_counter <= multipleFIFO_counter + 1;
----                if to_integer(unsigned(multipleFIFO_counter)) = 2 then 
----                    CS <= idle;
----                else
----                    count_ST0 <= (others => '0');
----                    CS <= ST0;
----                end if;
--        end case;
--    end if;
--end process;


multipleFIFO_address_gen : process(clock)
begin
    if rising_edge(clock) then
        multipleFIFO_address <= multipleFIFO_counter;
    end if;
end process;

end Behavioral;
