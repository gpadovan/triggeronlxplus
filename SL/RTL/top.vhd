-- project: SLReadout (created 21-11-21)
-- top.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operations on std_logic_vector
use IEEE.numeric_std.all;

library xil_defaultlib; -- FIFO library
use xil_defaultlib.my_library.all;

library work;
use work.all;


entity top is
  Port (clock_400MHz : in std_logic;
        clock_40MHz : in std_logic;
        enable : in std_logic;
        reset : in std_logic;
        data_in : in std_logic_vector(27 downto 0); 
        L0A : in std_logic;
        top_data_out : out std_logic_vector(18 downto 0) );
end top;


architecture Behavioral of top is

signal wait_FIFO_L0A_write_en : std_logic := '1';
signal wait_FIFO_L0A_read_en : std_logic := '1';
signal wait_FIFO_to_felix_write_en : std_logic := '1';
signal wait_FIFO_to_felix_read_en : std_logic := '1';
signal wait_write_en : std_logic := '1';
signal FIFO_L0A_write_en : std_logic := '0';
signal FIFO_L0A_read_en : std_logic := '0';
signal ser_FIFO_L0A_read_en : std_logic;
signal write_en : std_logic := '0';
signal trig_address : std_logic_vector(8 downto 0) := (others => '0');
signal ready_address : std_logic_vector(8 downto 0);
signal FIFO_L0A_full : std_logic := '0';
signal FIFO_L0A_empty : std_logic := '0';
signal SL_BCID : std_logic_vector(8 downto 0) := (others => '1');
signal address_rst : std_logic_vector(8 downto 0) := (others => '0');
signal TMP_wr_rst_busy : std_logic := '0';
signal TMP_rd_rst_busy : std_logic := '0';
signal multipleFIFO_address : std_logic_vector(4 downto 0);
signal memory_block_read_en : std_logic;
signal memory_block_FIFO_empty : std_logic;
signal memory_block_data_out : std_logic_vector(18 downto 0);
signal FIFO_to_felix_write_en : std_logic := '0';
signal FIFO_to_felix_read_en : std_logic := '1';
signal FIFO_to_felix_full : std_logic;
signal FIFO_to_felix_empty : std_logic;
signal FIFO_to_felix_wr_rst_busy : std_logic;
signal FIFO_to_felix_rd_rst_busy : std_logic;



component FIFO_L0A
  Port ( clk : IN STD_LOGIC;
         srst : IN STD_LOGIC;
         din : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
         wr_en : IN STD_LOGIC;
         rd_en : IN STD_LOGIC;
         dout : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
         full : OUT STD_LOGIC;
         empty : OUT STD_LOGIC;
         wr_rst_busy : OUT STD_LOGIC;
         rd_rst_busy : OUT STD_LOGIC );
end component;

component memory_block
    Port ( clock : in std_logic;
           reset : in std_logic;
           address_out : in std_logic_vector(8 downto 0);
           write_en : in std_logic;
           data_in : in std_logic_vector(18 downto 0);
           address_in : in std_logic_vector(8 downto 0);
           address_rst : in std_logic_vector(8 downto 0);
           multipleFIFO_address : in std_logic_vector(4 downto 0);
           memory_block_read_en : in std_logic;
           memory_block_FIFO_empty : out std_logic;
           memory_block_data_out : out std_logic_vector(18 downto 0) );
end component;

component serializer
    Port ( clock : in std_logic;
           FIFO_L0A_empty : in std_logic;
           ser_FIFO_L0A_read_en : out std_logic;
           multipleFIFO_address : out std_logic_vector(4 downto 0);
           memory_block_read_en : out std_logic;
           SL_BCID : in std_logic_vector(8 downto 0);
           memory_block_FIFO_empty : in std_logic );
end component;      

component FIFO_to_felix
    Port ( clk : IN STD_LOGIC;
           srst : IN STD_LOGIC;
           din : IN STD_LOGIC_VECTOR(18 DOWNTO 0);
           wr_en : IN STD_LOGIC;
           rd_en : IN STD_LOGIC;
           dout : OUT STD_LOGIC_VECTOR(18 DOWNTO 0);
           full : OUT STD_LOGIC;
           empty : OUT STD_LOGIC;
           wr_rst_busy : OUT STD_LOGIC;
           rd_rst_busy : OUT STD_LOGIC );
end component;


begin
     
  pmap_FIFO_L0A : FIFO_L0A port map (
      clk => clock_400MHz,
      srst => reset,
      din => trig_address,
      wr_en => FIFO_L0A_write_en,
      rd_en => FIFO_L0A_read_en,
      dout => ready_address,
      full=> FIFO_L0A_full,
      empty => FIFO_L0A_empty,
      wr_rst_busy => TMP_wr_rst_busy,
      rd_rst_busy => TMP_rd_rst_busy );
      
  pmap_memory_block : memory_block port map (
      clock => clock_400MHz,
      reset => reset,
      address_out => ready_address,
      write_en => write_en,
      data_in => data_in(18 downto 0),
      address_in => data_in(27 downto 19),
      address_rst => address_rst,
      multipleFIFO_address => multipleFIFO_address,
      memory_block_read_en => memory_block_read_en,
      memory_block_FIFO_empty => memory_block_FIFO_empty,
      memory_block_data_out => memory_block_data_out );
  
  pmap_serializer : serializer port map (
      clock => clock_400MHz,
      FIFO_L0A_empty => FIFO_L0A_empty,
      ser_FIFO_L0A_read_en => ser_FIFO_L0A_read_en,
      multipleFIFO_address => multipleFIFO_address,
      memory_block_read_en => memory_block_read_en,
      SL_BCID => SL_BCID,
      memory_block_FIFO_empty => memory_block_FIFO_empty );
      
  pmap_FIFO_to_felix : FIFO_to_felix port map (
      clk => clock_400MHz,
      srst => reset,
      din => memory_block_data_out,
      wr_en => FIFO_to_felix_write_en,
      rd_en => FIFO_to_felix_read_en,
      dout => top_data_out,
      full => FIFO_to_felix_full,
      empty => FIFO_to_felix_empty,
      wr_rst_busy => FIFO_to_felix_wr_rst_busy,
      rd_rst_busy => FIFO_to_felix_rd_rst_busy );     
      
           
  FIFO_L0A_write_en_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
      if reset = '1' or wait_FIFO_L0A_write_en = '1' or L0A = '0' then
        FIFO_L0A_write_en <= '0';
      else
        FIFO_L0A_write_en <= '1';
      end if;
    end if;
  end process;

  FIFO_L0A_read_en_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
        if reset = '1' or wait_FIFO_L0A_read_en = '1' then
            FIFO_L0A_read_en <= '0';
        else
            FIFO_L0A_read_en <= ser_FIFO_L0A_read_en; -- '1';
        end if;
    end if;
  end process;
  
  wait_FIFO_L0A_write_en_gen : process
  begin
    wait_FIFO_L0A_write_en <= '1';
    wait for 500 ns;
    wait_FIFO_L0A_write_en <= '0';
    wait;
  end process;  
  
  wait_FIFO_L0A_read_en_gen : process
  begin
    wait_FIFO_L0A_read_en <= '1';
    wait for 500 ns;
    wait_FIFO_L0A_read_en <= '0';
    wait;
  end process;
  
  FIFO_to_felix_write_en_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
        if reset = '1' or wait_FIFO_to_felix_write_en = '1' then
            FIFO_to_felix_write_en <= '0';
        else
            FIFO_to_felix_write_en <= '1';
        end if;
    end if;
  end process;

  FIFO_to_felix_read_en_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
        if reset = '1' or wait_FIFO_to_felix_read_en = '1' then
            FIFO_to_felix_read_en <= '0';
        else
            FIFO_to_felix_read_en <=  '1';
        end if;
    end if;
  end process;
  
  wait_FIFO_to_felix_write_en_gen : process
  begin
    wait_FIFO_to_felix_write_en <= '1';
    wait for 500 ns;
    wait_FIFO_to_felix_write_en <= '0';
    wait;
  end process;  
  
  wait_FIFO_to_felix_read_en_gen : process
  begin
    wait_FIFO_to_felix_read_en <= '1';
    wait for 500 ns;
    wait_FIFO_to_felix_read_en <= '0';
    wait;
  end process;
    
  write_en_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
        if reset = '1' or wait_write_en = '1' then
            write_en <= '0';
        else
            write_en <= '1';
        end if;
    end if;
  end process; 
  
  wait_write_en_gen : process
  begin
    wait_write_en <= '1';
    wait for 500 ns;
    wait_write_en <= '0';
    wait;
  end process;    

  SL_BCID_gen : process(clock_40MHz)
  begin
    if rising_edge(clock_40MHz) then
      if enable = '1' then
        SL_BCID <= SL_BCID + 1;
      end if;
    end if;
  end process;
  
  trig_address_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
      trig_address <= SL_BCID - std_logic_vector(to_unsigned(400, trig_address'length));
    end if;
  end process;
  
  address_rst_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
        address_rst <= SL_BCID - std_logic_vector(to_unsigned(480, address_rst'length));
    end if;
  end process;
  


end Behavioral;
