-- project SLReadout (created 21-11-2021)
-- decoder_2bit.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity decoder_2bit is
    Port ( clock : in std_logic;
           sgn_enc : in std_logic_vector(4 downto 0);
           sgn_dec : out std_logic_vector(25 downto 0) ); 
end decoder_2bit;

architecture Behavioral of decoder_2bit is

begin

    sgn_dec_gen : process(clock)
    begin
        if rising_edge(clock) then
            sgn_dec <= (others => '0');
                        
            for I in 0 to 25 loop
                if I = to_integer(unsigned(sgn_enc)) then
                    sgn_dec(I) <= '1';
                end if;
            end loop;
        
        end if;
    end process;
        
end Behavioral;
