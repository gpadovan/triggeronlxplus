-- project SLReadout
-- memory_block.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operation with std_logic_vector
use ieee.numeric_std.all;

library xil_defaultlib;
use xil_defaultlib.my_library.all;



entity memory_block is
    Port ( clock : in std_logic;
           reset : in std_logic;
           address_out : in std_logic_vector(8 downto 0);
           write_en : in std_logic;
           data_in : in std_logic_vector(18 downto 0);
           address_in : in std_logic_vector(8 downto 0);
           address_rst : in std_logic_vector(8 downto 0);
           multipleFIFO_address : in std_logic_vector(4 downto 0);
           memory_block_read_en : in std_logic;
           memory_block_FIFO_empty : out std_logic;
           memory_block_data_out : out std_logic_vector(18 downto 0) );
end memory_block;


architecture Behavioral of memory_block is
signal multipleFIFO_address_dec : std_logic_vector(25 downto 0);
signal multipleFIFO_read_en : std_logic_vector(25 downto 0);
signal multipleFIFO_FIFO_empty : std_logic_vector(25 downto 0);
signal multipleFIFO_data_out : data_bus_26;

component multipleFIFO
    Port ( clock : in std_logic;
           reset : in std_logic;
           address_out : in std_logic_vector(8 downto 0);
           global_write_en : in std_logic;
           data_in : in std_logic_vector(18 downto 0);
           address_in : in std_logic_vector(8 downto 0);
           address_rst : in std_logic_vector(8 downto 0);
           multipleFIFO_read_en : in std_logic;
           multipleFIFO_FIFO_empty : out std_logic;
           multipleFIFO_data_out : out std_logic_vector(18 downto 0) );
end component;

component decoder_2bit
    Port ( clock : in std_logic;
           sgn_enc : in std_logic_vector(4 downto 0);
           sgn_dec : out std_logic_vector(25 downto 0) ); 
end component;

begin

gen_multipleFIFO : for I in 0 to 25 generate
  multFIFO : multipleFIFO port map (
      clock => clock,
      reset => reset,
      address_out => address_out,
      global_write_en => write_en,
      data_in => data_in,
      address_in => address_in,
      address_rst => address_rst,
      multipleFIFO_read_en => multipleFIFO_read_en(I),
      multipleFIFO_FIFO_empty => multipleFIFO_FIFO_empty(I),
      multipleFIFO_data_out => multipleFIFO_data_out(I) );
end generate gen_multipleFIFO;

pmap_decoder_2bit : decoder_2bit port map (
    clock => clock,
    sgn_enc => multipleFIFO_address, 
    sgn_dec => multipleFIFO_address_dec ); 

multipleFIFO_read_en_gen : process(clock)
begin
    if rising_edge(clock) then
        for I in 0 to 25 loop
            multipleFIFO_read_en(I) <= memory_block_read_en and multipleFIFO_address_dec(I);
        end loop;
    end if;
end process;


multiplexer_multipleFIFO_FIFO_empty : process(clock)
begin
    if rising_edge(clock) then
        for I in 0 to 25 loop
            if I = to_integer(unsigned(multipleFIFO_address)) then
                memory_block_FIFO_empty <= multipleFIFO_FIFO_empty(I);
            end if;
        end loop; 
    end if;
end process;

multiplexer_multipleFIFO_data_out : process(clock)
begin
    if rising_edge(clock) then
        for I in 0 to 25 loop
            if I = to_integer(unsigned(multipleFIFO_address)) then
                memory_block_data_out <= multipleFIFO_data_out(I);
            end if;
        end loop;
    end if;
end process;




end Behavioral;
