-- project SLReadout (created on 21-11-2021)
-- data_generator.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity data_generator is
    Port (clock : in std_logic;
          enable : in std_logic;
          data_out : out std_logic_vector(27 downto 0) );
end data_generator;

architecture Behavioral of data_generator is

type state_type is (idle, ST0, ST1);
signal CS : state_type := idle;
signal BCID : std_logic_vector(8 downto 0) := (others => '0');
signal HIT : std_logic_vector (18 downto 0) := "0000000000000000001";
signal hit_count : std_logic_vector(3 downto 0) := (others => '0');
signal empty_count : std_logic_vector(3 downto 0) := (others => '0');

begin

    seq_proc : process(clock)
    begin
        if rising_edge(clock) then
            case CS is 
                when idle => 
                    data_out <= (others => '0');
                        if enable = '1' then
                            CS <= ST0;
                            hit_count <= (others => '0');
                        end if;
                when ST0 =>
                    HIT <= HIT + 1;
                    data_out <= BCID & HIT;
                    if to_integer(unsigned(hit_count)) < 4 then
                        hit_count <= hit_count + 1;
                    else
                        empty_count <= (others => '0');
                        CS <= ST1;
                    end if;
                    
                when ST1 =>
                    data_out <= BCID & "0000000000000000000";
                    if to_integer(unsigned(empty_count)) < 4 then
                        empty_count <= empty_count + 1;
                    else
                        BCID <= BCID + 1;
                        hit_count <= (others => '0');
                        CS <= ST0;
                    end if;
              
            end case;
        end if;
    end process;        


end Behavioral;
