-- project SLReadout (created on 21-11-2021)
-- L0A_generator.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity L0A_generator is
    Port (clock : in std_logic;
          trigger_rate : in std_logic;
          L0A : out std_logic );
end L0A_generator;


architecture Behavioral of L0A_generator is

begin
    L0A_gen : process(clock)
    begin
        if rising_edge(clock) then
            if rising_edge(trigger_rate) then
                L0A <= '1';
             else
                L0A <= '0';
             end if;
         end if;
    end process; 

end Behavioral;
