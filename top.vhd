-- project: multipleFIFO
-- top.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operations on std_logic_vector
use IEEE.numeric_std.all;

library xil_defaultlib; -- FIFO library
use xil_defaultlib.my_lib.all;

library work;
use work.all;


entity top is
  Port (clock_400MHz : in std_logic;
        clock_40MHz : in std_logic;
        reset : in std_logic;
        enable : in std_logic;
        data_in : in std_logic_vector(27 downto 0) := (others => '0');
        L0A : in std_logic;
        top_out : out std_logic_vector(31 downto 0) );
end top;

architecture Behavioral of top is
    
  signal write_en : std_logic := '0';
  signal read_en : std_logic :='0';
  signal wait_write_en : std_logic := '1';
  signal wait_read_en : std_logic := '1';
  signal SL_BCID : std_logic_vector(8 downto 0) := (others => '1');
  signal SL_BCID_sync : std_logic_vector(8 downto 0) := (others=> '0');
  signal mFIFO_empty : std_logic_vector(511 downto 0) := (others => '0'); 
  signal mFIFO_read_en : std_logic_vector(511 downto 0) := (others => '0');

  signal mFIFO_data : data_bus;


component multipleFIFO
    Port ( global_write_en : in std_logic;
           global_read_en : in std_logic;
           data_in : in std_logic_vector(18 downto 0);
           data_out : out data_bus; -- questo segnale deve diventare UN SOLO vector che continene la sequenza di dati uscente dalla FIFO selezionata
           reset : in std_logic;
           clock : in std_logic;
           SL_BCID : in std_logic_vector(8 downto 0);
           address_in : in std_logic_vector(8 downto 0);
           mFIFO_empty: out std_logic_vector(511 downto 0);
           mFIFO_read_en : in std_logic_vector(511 downto 0) );
end component;

component serializer
    Port ( clock : in std_logic;
           L0A : in std_logic;
           SL_BCID : in std_logic_vector(8 downto 0);
           mFIFO_empty : in std_logic_vector(511 downto 0);
           mFIFO_read_en : out std_logic_vector(511 downto 0);
           data_in : in data_bus );
end component;


begin

  pmap_multipleFIFO : multipleFIFO port map (
       global_write_en => write_en,
       global_read_en => read_en,
       data_in => data_in(18 downto 0),
       data_out => mFIFO_data,
       reset => reset,
       clock => clock_400MHz,
       SL_BCID => SL_BCID_sync,
       address_in => data_in(27 downto 19),
       mFIFO_empty => mFIFO_empty,
       mFIFO_read_en => mFIFO_read_en );
       
  pmap_serializer : serializer port map (
       clock => clock_400MHz,
       L0A => L0A,
       SL_BCID => SL_BCID_sync,
       mFIFO_empty => mFIFO_empty,
       mFIFO_read_en => mFIFO_read_en,
       data_in => mFIFO_data );
       

  write_en_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
      if reset = '1' or wait_write_en = '1' then
        write_en <= '0';
      else
        write_en <= '1';
      end if;
    end if;
  end process;


  read_en_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
      if reset = '1' or wait_read_en = '1' then
        read_en <= '0';
      else
        read_en <= '1';
      end if;
    end if;
  end process;
  
  wait_write_en_gen : process
  begin
    wait_write_en <= '1';
    wait for 500 ns;
    wait_write_en <= '0';
    wait;
  end process;
  
  wait_read_en_gen : process
  begin
    wait_read_en <= '1';
    wait for 10500 ns; -- 500ns + 10 mus
    wait_read_en <= '0';
    wait;
  end process;
  
  SL_BCID_gen : process(clock_40MHz)
  begin
    if rising_edge(clock_40MHz) then
        if enable = '1' then
            SL_BCID <= SL_BCID + 1;
        end if;
    end if;
  end process;
  
  SL_BCID_sync_gen : process(clock_400MHz)
  begin
    if rising_edge(clock_400MHz) then
        SL_BCID_sync <= SL_BCID;
    end if;
  end process;
    
  

end Behavioral;
